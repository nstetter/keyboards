/*
Copyright 2017 Danny Nguyen <danny@keeb.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// #define USE_I2C
#define USE_SERIAL

// SPLIT settings
#define EE_HANDS
#define SPLIT_LAYER_STATE_ENABLE

//required for IBus versions ≥1.5.15, where Ctrl+Shift+U behavior is consolidated into Ctrl+Shift+E
//#define UNICODE_KEY_LNX  LCTL(LSFT(KC_E))

//number of times a modifier has to be pressed to toggle a layer
#define TAPPING_TOGGLE 2

//RGBLIGHT settings
#define RGBLIGHT_LAYERS
// #undef RGBLED_NUM
#undef RGBLIGHT_ANIMATIONS
#define RGBLIGHT_HUE_STEP 8
#define RGBLIGHT_SAT_STEP 8
#define RGBLIGHT_VAL_STEP 8

#define RGBLIGHT_LIMIT_VAL 255

//maximum brightness for the breathing mode
#define RGBLIGHT_EFFECT_BREATHE_MAX 75

// #undef RGBLIGHT_SPLIT

// save space and make EXTRAFLAGS -flto possible
#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION

//disable debugging to save more space
#ifndef NO_DEBUG
#define NO_DEBUG
#endif // !NO_DEBUG
#if !defined(NO_PRINT) && !defined(CONSOLE_ENABLE)
#define NO_PRINT
#endif // !NO_PRINT
