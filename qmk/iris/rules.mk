#HOWTO build
#make keebio/iris/rev3:namron:flash

BOOTLOADER = dfu

#reduce build size
EXTRAFLAGS += -flto

RGBLIGHT_ENABLE = yes
BACKLIGHT_ENABLE = yes
MOUSEKEY_ENABLE = yes
TAP_DANCE_ENABLE=yes
#UNICODEMAP_ENABLE = yes
#UNICODE_ENABLE = yes
