#HOWTO build
#make kyria:namron:flash

BOOTLOADER = qmk-dfu

#reduce build size
EXTRAFLAGS += -flto

AUDIO_ENABLE = no          # Audio output on port C6
BACKLIGHT_ENABLE = no      # Enable keyboard backlight functionality on B7 by default
BLUETOOTH_ENABLE = no      # Enable Bluetooth with the Adafruit EZ-Key HID
BOOTMAGIC_ENABLE = no      # Disable BootMagic
COMMAND_ENABLE = no        # Commands for debug and configuration
CONSOLE_ENABLE = no        # Console for debug
ENCODER_ENABLE = yes       # Enables the use of one or more encoders
EXTRAKEY_ENABLE = yes      # Audio control and System control
FAUXCLICKY_ENABLE = no     # Use buzzer to emulate clicky switches
HD44780_ENABLE = no        # Enable support for HD44780 based LCDs
MIDI_ENABLE = no           # MIDI support
MOUSEKEY_ENABLE = yes      # Mouse keys
NKRO_ENABLE = no           # USB Nkey Rollover
OLED_DRIVER = SSD1306      # Enables the use of OLED displays
OLED_ENABLE = yes
RGBLIGHT_ENABLE = yes      # Enable keyboard RGB underglow
TAP_DANCE_ENABLE=yes
UNICODE_ENABLE = no        # Unicode

SPLIT_TRANSPORT = mirror
