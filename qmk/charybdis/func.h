#pragma once

#include "quantum.h"

#include "features/achordion.h" //ACHORDION

// Umlauts
// Use Layout "US international, with dead keys"


/* -------------------------------------------------------------------------- */
/*                                   DEFINES                                  */
/* -------------------------------------------------------------------------- */

/* ---------------------------------- keys ---------------------------------- */
// home row mods
// linux-left
#define MOD_SFT_A LSFT_T(KC_A)
#define MOD_GUI_R LGUI_T(KC_R)
#define MOD_CTL_S LCTL_T(KC_S)
#define MOD_ALT_T LALT_T(KC_T)
//linux-right
#define MOD_ALT_N LALT_T(KC_N)
#define MOD_CTL_E RCTL_T(KC_E)
#define MOD_GUI_I RGUI_T(KC_I)
#define MOD_SFT_O RSFT_T(KC_O)

//mac-left
#define MOD_CTL_R LCTL_T(KC_R)
#define MOD_GUI_S LGUI_T(KC_S)
//mac-right
#define MOD_GUI_E RGUI_T(KC_E)
#define MOD_CTL_I RCTL_T(KC_I)

// layer modifiers
#define SP_NAV LT(_NAV,KC_SPC) //SPACE when tapped _NAV layer when hold
#define SP_ESCINT LT(_INT,KC_ESC) //ESC when tapped, _INT layer when hold
#define SP_SHBS LSFT_T(KC_BSPC) //Backspace when tapped, Shift when hold
#define SP_FN LT(_FN,KC_DEL) //DEL when tapped, _FN when hold

/* -------------------------------------------------------------------------- */
/*                                  FUNCTIONS                                 */
/* -------------------------------------------------------------------------- */
// function to send single tap without modifiers
void tap_code_nomod(int keycode) {
  uint8_t temp_mods = get_mods();
  clear_mods();
  tap_code(keycode);
  set_mods(temp_mods);
}

// function to register keycode without modifiers
void register_code_nomod(int keycode) {
  uint8_t temp_mods = get_mods();
  clear_mods();
  register_code(keycode);
  set_mods(temp_mods);
}

// function to input umlauts on Mac using ALT+U combo
void tap_umlaut_mac(int keycode) {
  uint8_t temp_mods = get_mods();
  clear_mods();
  tap_code16(LOPT(KC_U));
  set_mods(temp_mods);
  tap_code(keycode);
}

enum layer_names {
  _COLEMAK,
  _MAC,
  _INT,
  _NAV,
  _MOUSE,
  _FN,
};

enum custom_keycodes {
  MOD0 = SAFE_RANGE,
  MOD1,
  MOD2,
  MOD3,
  ND_GRV,
  ND_QUOTE,
  TG_DFT,
  DE_ae,
  DE_oe,
  DE_ss,
  DE_ue,
  DE_eur,
  SP_COPY,
  SP_CUT,
  SP_PASTE
};

/* -------------------------------------------------------------------------- */
/*                                INIT/WAKEUP                                 */
/* -------------------------------------------------------------------------- */


void suspend_wakeup_init_kb(void) {
  // re-enable rgb matrix on wakeup
     #ifdef RGB_MATRIX_ENABLE
          rgb_matrix_set_suspend_state(false);
     #endif
     suspend_wakeup_init_user();
}

void sync_layer_state(void) {
  //sync layer_state and default_layer_state (from EEPORM) on init
  layer_state_set(default_layer_state);
}

void suspend_wakeup_init_user(void) {
  //sync layer_state and default_layer_state (from EEPORM) on init
  // layer_state_set(default_layer_state);
  sync_layer_state();
}

// auto-mouse: https://github.com/qmk/qmk_firmware/pull/17962
void pointing_device_init_user(void) {
    set_auto_mouse_layer(_MOUSE); // only required if AUTO_MOUSE_DEFAULT_LAYER is not set to index of <mouse_layer>
    set_auto_mouse_enable(true);         // always required before the auto mouse feature will work
}

// void matrix_init_user(void) {
//   //sync layer_state and default_layer_state (from EEPORM) on init
//   layer_state_set(default_layer_state);
// }

/* -------------------------------------------------------------------------- */
/*                                  SHUTDOWN                                  */
/* -------------------------------------------------------------------------- */
void suspend_power_down_kb(void)
{
     #ifdef RGB_MATRIX_ENABLE
          rgb_matrix_set_suspend_state(true);
     #endif
}

/* -------------------------------------------------------------------------- */
/*                                 USER INPUT                                 */
/* -------------------------------------------------------------------------- */
bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  if (!process_achordion(keycode, record)) { return false; } // ACHORDION

  switch (keycode) {
    case KC_NO:
      /* Always cancel one-shot layer when another key gets pressed */
      if (record->event.pressed && is_oneshot_layer_active())
      clear_oneshot_layer_state(ONESHOT_OTHER_KEY_PRESSED);
      return true;
    case MOD0:
      if (record->event.pressed) {
        set_single_persistent_default_layer(0);
      }
      return false;
      break;

    // Toggle default layers between _COLEMAK and _MAC
    case TG_DFT:
      if (record->event.pressed) {
        if ( IS_LAYER_OFF(_MAC)) {
          layer_on(_MAC);
          set_single_persistent_default_layer(_MAC);
        }
        else if (IS_LAYER_ON(_MAC)) {
          layer_off(_MAC);
          set_single_persistent_default_layer(0);
        }
      }
      return false;
      break;

    // Tap Space after international layout dead keys, to use them like regular keys
    case ND_GRV:
      if (record->event.pressed) {
        tap_code(KC_GRV);
        tap_code_nomod(KC_SPC);
      } else {
        //
      }
      return false;
      break;
    case ND_QUOTE:
      if (record->event.pressed) {
        tap_code(KC_QUOT);
        tap_code_nomod(KC_SPC);
      } else {
        //
      }
      return false;
      break;
    case KC_6:
      if (record->event.pressed) {
        if (get_mods() & MOD_MASK_SHIFT) { //send space after keycode if SHIFT is pressed, to avoid dead key for ^
            tap_code(KC_6);
            tap_code_nomod(KC_SPC);
        } else {
            tap_code(KC_6);
        }
      } else {
        //
      }
      return false;
      break;
    case DE_ae:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_umlaut_mac(KC_A);
        } else {
          tap_code16(RALT(KC_Q));
        }
      }
      return false;
      break;
    case DE_oe:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_umlaut_mac(KC_O);
        } else {
          tap_code16(RALT(KC_P));
        }
      }
      return false;
      break;
    case DE_ue:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_umlaut_mac(KC_U);
        } else {
          tap_code16(RALT(KC_Y));
        }
      }
      return false;
      break;
    case DE_ss:
      if (record->event.pressed) {
        tap_code16(RALT(KC_S));
      }
      return false;
      break;
    case DE_eur:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_code16(LSFT(RALT(KC_2)));
        } else {
        tap_code16(RALT(KC_5));
        }
      }
      return false;
      break;
    case SP_COPY:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_code16(LGUI(KC_C));
        } else {
        tap_code16(LCTL(KC_C));
        }
      }
      return false;
      break;
    case SP_CUT:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_code16(LGUI(KC_X));
        } else {
        tap_code16(LCTL(KC_X));
        }
      }
      return false;
      break;
    case SP_PASTE:
      if (record->event.pressed) {
        if (IS_LAYER_ON(_MAC)) {
          tap_code16(LGUI(KC_V));
        } else {
        tap_code16(LCTL(KC_V));
        }
      }
      return false;
      break;
    }
  return true;
}

uint16_t get_quick_tap_term(uint16_t keycode, keyrecord_t* record) {
  // If you quickly hold a tap-hold key after tapping it, the tap action is
  // repeated. Key repeating is useful e.g. for Vim navigation keys, but can
  // lead to missed triggers in fast typing. Here, returning 0 means we
  // instead want to "force hold" and disable key repeating.
  switch (keycode) {
    case SP_SHBS:
      return QUICK_TAP_TERM;  // Enable key repeating.
    case SP_FN:
      return QUICK_TAP_TERM;  // Enable key repeating.
    default:
      return 0;  // Otherwise, force hold and disable key repeating.
  }
}

/* -------------------------------------------------------------------------- */
/*                                 ACHORDION                                  */
/* -------------------------------------------------------------------------- */
void matrix_scan_user(void) {
  achordion_task(); //ACHORDION
}

bool achordion_chord(uint16_t tap_hold_keycode, keyrecord_t* tap_hold_record, uint16_t other_keycode, keyrecord_t* other_record) {
  // Disable opposite hands rule for thumb keys. Thumb keys are on row 3 and 7,
  // so we use % to calculate the remainder. MATRIX_ROWS/2 because of split layout.
  // Either if the thumb key is the tap-hold or other key
  if (tap_hold_record->event.key.row % (MATRIX_ROWS / 2) >= 3) { return true; }
  if (other_record->event.key.row % (MATRIX_ROWS / 2) >= 3) { return true; }

  // Allow same-hand holds with non-alpha keys.
  switch (other_keycode) {
    case QK_MOD_TAP ... QK_MOD_TAP_MAX: // ... is range here
    case QK_LAYER_TAP ... QK_LAYER_TAP_MAX:
      other_keycode &= 0xff;  // Get base keycode.
  }
  if (other_keycode > KC_Z) { return true; }
  
  // Otherwise, follow the opposite hands rule.
  return achordion_opposite_hands(tap_hold_record, other_record);
}

uint16_t achordion_timeout(uint16_t tap_hold_keycode) {
  switch (tap_hold_keycode) {
    case SP_SHBS:
    case SP_NAV:
      return 0;  // Completely bypass Achordion for these keys.
  }

  return 750;  // Otherwise use a this timeout.
}

bool achordion_eager_mod(uint8_t mod) {
  switch (mod) {
    case MOD_LSFT:
      return true;
    case MOD_LCTL:
      return true;
    case MOD_LGUI:
      if (IS_LAYER_ON(_MAC)) {
        return true;
      }
    default:
      return false;
  }
}

uint16_t achordion_streak_timeout(uint16_t tap_hold_keycode) {
  return 100;  // Default of 100 ms.
}
