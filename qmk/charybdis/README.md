## Usage

### compile
```
qmk compile -c -kb bastardkb/charybdis/3x6/v2/splinky_3 -km namron

```

### flash
```
qmk flash -c -kb bastardkb/charybdis/3x6/v2/splinky_3 -km namron

```
