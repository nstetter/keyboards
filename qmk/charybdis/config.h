//number of times a modifier has to be pressed to toggle a layer
#define TAPPING_TOGGLE 2

// Configure the global tapping term (default: 200ms)
#define TAPPING_TERM 175

// Enable tap term per key, configured in get_quick_tap_term
#define QUICK_TAP_TERM_PER_KEY
#define PERMISSIVE_HOLD
#define ACHORDION_STREAK

// Auto-mouse-layer https://github.com/qmk/qmk_firmware/pull/17962
#define POINTING_DEVICE_AUTO_MOUSE_ENABLE

//natural scrolling
#define CHARYBDIS_DRAGSCROLL_REVERSE_Y

#define MOUSE_EXTENDED_REPORT


#undef RP2040_BOOTLOADER_DOUBLE_TAP_RESET_TIMEOUT
#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET_TIMEOUT 400U

// RGB Matrix

#undef RGB_MATRIX_MAXIMUM_BRIGHTNESS
#define RGB_MATRIX_MAXIMUM_BRIGHTNESS 100
// #undef RGB_MATRIX_STARTUP_MODE
// #define RGB_MATRIX_STARTUP_MODE RGB_MATRIX_BREATHING
// #undef RGB_MATRIX_STARTUP_HSV
// #define RGB_MATRIX_STARTUP_HSV

// sync layer state for layer indication
#define SPLIT_LAYER_STATE_ENABLE
// #define SPLIT_TRANSPORT_MIRROR

#define USB_SUSPEND_WAKEUP_DELAY 250
#define SPLIT_USB_TIMEOUT 1000
#define SPLIT_WATCHDOG_ENABLE
#define SPLIT_WATCHDOG_TIMEOUT 1500
