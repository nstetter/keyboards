# Overview

This repo contains QMK keyboard firmware for my currently in use keyboards and also keyboard definitions for [klfc](https://github.com/39aldo39/klfc).

## QMK

QMK firmware is used for all keyboards. Right now these are [Keebio Iris (rev3)](https://keeb.io/products/iris-keyboard-split-ergonomic-keyboard) and [splitkb kyria](https://blog.splitkb.com/blog/introducing-the-kyria).

Layout overviews are generated using [keyboard-layout-editor](http://www.keyboard-layout-editor.com).


### Setup

1. Install qmk (qmk from AUR), or clone the [qmk repo](https://github.com/qmk/qmk_firmware) and follow the [install instructions](https://beta.docs.qmk.fm/tutorial/newbs_getting_started).

1. Link the files from this repo into the qmk codebase:

```sh
mkdir $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/users/namron/ && ln -s "~/repos/nstetter/keyboards/qmk/common/config.h" $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/users/namron/

mkdir $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/splitkb/kyria/keymaps/namron && ln -s ~/repos/nstetter/keyboards/qmk/kyria/* $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/splitkb/kyria/keymaps/namron

mkdir $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/keebio/iris/keymaps/namron/ && ln -s ~/repos/nstetter/keyboards/qmk/iris/* $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/keebio/iris/keymaps/namron/

mkdir $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/bastardkb/charybdis/3x6/keymaps/namron/ && ln -s ~/repos/nstetter/keyboards/qmk/charybdis/* $(qmk env | grep -oP "(?<=QMK_HOME=\").*?(?=\")")/keyboards/bastardkb/charybdis/3x6/keymaps/namron/ 
```

### Build the firmware

```sh
qmk compile -kb keebio/iris/rev3 -km namron
qmk compile -kb splitkb/kyria -km namron
qmk compile -c -kb bastardkb/charybdis/3x6/v2/splinky_3 -km namron
```

### Flash the fimware

```sh
qmk flash -kb keebio/iris/rev3 -km namron -bl dfu
qmk flash -kb splitkb/kyria -km namron -bl dfu
qmk flash -c -kb bastardkb/charybdis/3x6/v2/splinky_3 -km namron
```

## KLFC

TODO
